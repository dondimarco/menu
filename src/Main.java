package com.company;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.IOException;
import java.util.Scanner;

public class Main implements Runnable {

    Scanner in=new Scanner(System.in);
    public static Screen screen;
    private static Display display;
    private int frames=0;
    private boolean running=false;
    private final int TPS=20;
    private Thread thread;
    private BufferStrategy bs;
    private Graphics g;


    public static void main(String[] args) {new Main().start();} //MAIN

    public void tick() { //PARTE LOGICA DEL PROGRAMMA

       screen.tick();

        display.getCanvas().addMouseListener(screen);
        display.getCanvas().addMouseMotionListener(screen);
        display.getFrame().addKeyListener(screen);
    }

    public void render()  //PARTE GRAFICA
    {
        bs = display.getCanvas().getBufferStrategy();
        if(bs==null)
        {
            display.getCanvas().createBufferStrategy(5);
            return;
        }

        g = bs.getDrawGraphics();
        screen.render(g);
        bs.show();
        g.dispose();

    }

    public void init() throws IOException, FontFormatException {

        //Display
        Assets.init();
        display = new Display("Menu", 1280, 720);  //SETTO FINESTRA
        screen = new MainMenu();

    }

    //----------------------------------------------------------------------------------------------------------------------------//

    @Override
    public void run() {

        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FontFormatException e) {
            e.printStackTrace();
        }
        double timePerTick = 1000000000 / TPS;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        while(running) {
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;
            if (delta >= 1) {
                tick();
                delta--;
            }
            render();
            frames++;

            if (timer >= 1000000000) {
                //System.out.println(frames);
                frames = 0;
                timer = 0;
            }
        }
    }

    public synchronized void start()
    {   if(running)
        {
            return;
        }


        running=true;
        thread=new Thread(this);
        thread.start();

    }

    public synchronized void stop()
    {
        if(!running)
            return;
        running=false;
        try
        {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Display getDisplay() {
        return display;
    }


}