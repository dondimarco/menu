package com.company;

/**
 * QUESTA CLASSE SI OCCUPA DELL'INSERIMENTO DELL'IMMAGINE
 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Image
{

    public static BufferedImage loadImage(String path)
    {
        try {
            return ImageIO.read(Image.class.getResource("..\\..\\textures\\" + path));
        }
            catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }

        return null;
    }


}
