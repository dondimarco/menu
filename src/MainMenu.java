package com.company;

import java.awt.*;


public class MainMenu extends Menu {

    public MainMenu(){

        //INSERIMENTO BOTTONI
        buttons.add(new Button(1280/2-100,200,200,80));  //SETTO DIMENSIONI BOTTONE START
        buttons.add(new Button(1280/2-100,300,200,80)); //SETTO DIMENSIONI BOTTONE OPZIONI
        buttons.add(new Button(1280/2-100,400,200,80)); //SETTO DIMENSIONI BOTTONE EXIT
    }

     @Override
    public void tick()
    {
        buttons.get(0).tick();
        buttons.get(1).tick();
        buttons.get(2).tick();

        if(buttons.get(1).isHover() && clicked)
        {
            Main.screen = new Option();
        }

        if(buttons.get(2).isHover() && clicked)
        {
            System.exit(0); //EXIT
        }

        clicked = false;
    }


    @Override
    public void render(Graphics g)
    {
        g.fillRect(0,0,Display.width, Display.height); //INSERISCO RECTANGLES
        g.drawImage(Assets.background,0,0,Display.width,Display.height,null); //INSERISCO L'IMMAGINE

        //TITOLO

        g.setFont(Assets.font);
        g.drawString("BIOMONI", Display.width/2-315, 140);

        buttons.get(0).render(g);

        g.setFont(Assets.font.deriveFont(24f));
        g.setColor(Color.WHITE);
        g.drawString("@Powered by Biomoni", 10, 710);

        //START
        g.setFont(Assets.font.deriveFont(46f));
        g.setColor(Color.BLACK);
        g.drawString("START", Display.width/2-82, 255);

        buttons.get(1).render(g);

        //OPTION
        g.setFont(Assets.font.deriveFont(46f));
        g.setColor(Color.BLACK);
        g.drawString("OPTION", Display.width/2-90, 355);

        buttons.get(2).render(g);

        //EXIT
        g.setFont(Assets.font.deriveFont(46f));
        g.setColor(Color.BLACK);
        g.drawString("EXIT", Display.width/2-55, 455);
    }


}
