package com.company;

/**
 * QUESTA CLASSE SI OCCUPA DEL FUNZIONAMENTO DEI BOTTONI E RETTANGOLI
 */

import java.awt.*;

public class Button {

    private boolean hover, active = false;
    private Rectangle button;
    private Color color = Color.BLUE;

    public boolean isHover() {
        return hover;
    }

    public Button(int x, int y, int width, int height)
    {
        button = new Rectangle(x,y,width,height);
    }

    public Rectangle getButton()
    {
        return button;
    }

    public Color getColor()
    {
        return color;
    }

    public void toggleActive(){
        active =! active;
    }

    public void tick() //PARTE LOGICA
    {
        if(active)
        {
            color = Color.RED;
        }
        else if(button.contains(Menu.mouseX, Menu.mouseY))
        {
            color=Color.RED;
            hover = true;
        }
        else
        {
            color=Color.WHITE;
            hover = false;
        }

    }
    public void render(Graphics g) //PARTE GRAFICA
    {
        g.setColor(color);
        g.fillRect( (int) button.getX(), (int)  button.getY(),  (int) button.getWidth(), (int) button.getHeight());
    }
}
