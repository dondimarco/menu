package com.company;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Option extends Menu
{
    public static int musicVolume = 10;
    public static int soundVolume = 10;
    public static int animalVolume = 10;
    private int musicChanger;
    private int soundChanger;
    private int animalChanger;



    public Option(){

        buttons.add(new Button(1100,180,50,50));
        buttons.add(new Button(1100,320,50,50));
        buttons.add(new Button(1100, 450,50, 50));


    }
    @Override
    public void tick()
    {
        //controllo
        if(musicVolume+musicChanger >= 0 && musicVolume+musicChanger <=10)
        {
            musicVolume += musicChanger;
        }
        musicChanger = 0;

        if(soundVolume+soundChanger >= 0 && soundVolume+soundChanger <=10)
        {
            soundVolume += soundChanger;
        }
        soundChanger = 0;

        if(animalVolume+animalChanger >= 0 && animalVolume+animalChanger <= 10)
        {
            animalVolume += animalChanger;
        }
        animalChanger = 0;

        buttons.get(0).tick();
        buttons.get(1).tick();
        buttons.get(2).tick();

        if(buttons.get(0).isHover() && clicked)
        {
            buttons.get(0).toggleActive();
        }

        if(buttons.get(1).isHover() && clicked)
        {
            buttons.get(1).toggleActive();
        }

        if(buttons.get(2).isHover() && clicked)
        {
            buttons.get(2).toggleActive();
        }

        clicked = false;

    }
    @Override
    public void render(Graphics g)
    {
        g.fillRect(0,0,Display.width, Display.height);
        g.drawImage(Assets.backgroundOption,0,0,Display.width,Display.height,null); //INSERISCO L'IMMAGINE

        g.setFont(Assets.font.deriveFont(50f));
        g.setColor(Color.RED);
        g.drawString("PRESS 'ESC' TO RETURN TO THE MAIN MENU'", 35, 80);

        //GAME MUSIC

        g.setFont(Assets.font.deriveFont(40f));
        g.setColor(Color.BLACK);
        g.drawString("GAME MUSIC", 150, 170);

        g.setColor(Color.RED);
        for (int i = 0; i < musicVolume; i++) {
            g.fillRect(100 + (10 + 30) * i, 180, 30, 50);
        }

        //SOUND MUSIC

        g.setFont(Assets.font.deriveFont(40f));
        g.setColor(Color.BLACK);
        g.drawString("SOUND MUSIC", 145, 310);

        g.setColor(Color.RED);
        for (int i = 0; i < soundVolume; i++) {
            g.fillRect(100 + (10 + 30) * i, 320, 30, 50);
        }

        //ANIMAL SOUND

        g.setFont(Assets.font.deriveFont(40f));
        g.setColor(Color.BLACK);
        g.drawString("ANIMAL SOUND", 130, 450);

        g.setColor(Color.RED);
        for (int i = 0; i < animalVolume; i++) {
            g.fillRect(100 + (10 + 30) * i, 460, 30, 50);
        }

        //PULSANTE MUTE 1

        g.setFont(Assets.font.deriveFont(40f));
        g.setColor(Color.BLACK);
        g.drawString("MUTE", 960, 220);

        g.setColor(Color.white);
        buttons.get(0).render(g);

        //PULSANTE MUTE 2

        g.setFont(Assets.font.deriveFont(40f));
        g.setColor(Color.BLACK);
        g.drawString("MUTE", 960, 360);

        g.setColor(Color.WHITE);
        buttons.get(1).render(g);

        //PULSANTE MUTE 3

        g.setFont(Assets.font.deriveFont(40f));
        g.setColor(Color.BLACK);
        g.drawString("MUTE", 960, 490);

        g.setColor(Color.white);
        buttons.get(2).render(g);
    }

    @Override
    public void keyPressed(KeyEvent e) {

        //SE IL TASTO PREMUTO E' ESC TORNA AL MENU' PRINCIPALE

        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            Main.screen = new MainMenu();
        }

        if(e.getKeyCode() == KeyEvent.VK_RIGHT)
        {
            musicChanger = 1;
        }
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
        {
            musicChanger = -1;
        }

        if(e.getKeyCode() == KeyEvent.VK_UP)
        {
            soundChanger = 1;
        }
        if(e.getKeyCode() == KeyEvent.VK_DOWN)
        {
            soundChanger = -1;
        }

        if(e.getKeyCode() == KeyEvent.VK_N)
        {
            animalChanger = 1;
        }
        if(e.getKeyCode() == KeyEvent.VK_M)
        {
            animalChanger = -1;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
    }
}
