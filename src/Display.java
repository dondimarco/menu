package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Display{

    private JFrame frame;
    private Canvas canvas;
    private String title;
    public static int width, height;

    public Display(String title, int width, int height)
    {
        this.title=title;
        this.width=width;
        this.height=height;
        createWindow();
    }

    private void createWindow(){

        frame = new JFrame(title);
        frame.setSize(width,height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setMinimumSize(new Dimension(width,height));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(width,height));
        canvas.setMinimumSize(new Dimension(width,height));
        canvas.setFocusable(false);

        frame.add(canvas);
        frame.pack();

    }

    public Canvas getCanvas()
    {
        return canvas;
    }
    public JFrame getFrame()
    {
        return frame;
    }

}

