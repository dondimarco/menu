package com.company;

//CLASSE CHE SI OCCUPA DI CARICARE FONT E IMMAGINI

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Assets {

    public static Font font;
    public static BufferedImage background;
    public static BufferedImage backgroundOption;

    public static void init() throws IOException, FontFormatException {

        font = Font.createFont(Font.TRUETYPE_FONT, Assets.class.getResourceAsStream("..\\..\\fonts\\SuperMario256.ttf")).deriveFont(140f);
        background = Image.loadImage("grass2.jpg");
        backgroundOption = Image.loadImage("sfondoOption2.png");
    }
}
