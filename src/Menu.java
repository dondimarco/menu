package com.company;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

abstract class Menu extends Screen{

    public static int mouseX=0,mouseY=0;
    protected boolean clicked = false;
    protected ArrayList<Button> buttons=new ArrayList<>();

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX=e.getX();
        mouseY=e.getY();
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1)
            clicked = true;

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
